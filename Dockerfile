FROM centos:7
MAINTAINER matthewbednarski@gmail.com

RUN rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
RUN yum -y install epel-release && rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm

RUN yum -y install dbus-x11
RUN yum -y install gnucash
RUN yum -y install libdbi-dbd-mysql libdbi-dbd-pgsql libdbi-dbd-sqlite

ENTRYPOINT ["/usr/bin/gnucash"]